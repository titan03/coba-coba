const app = require('../app')
const request = require('supertest')
const { User } = require('../models')

let getToken

class UserTest {
  constructor(name, email, password){
    this.name = name
    this.email = email
    this.password = password
  }
}

let user1 = new UserTest('Rasyid','rasyid@test.com','12345')
let user2 = new UserTest('Titanio','titanio@test.com','12345')

describe('User API Collection', () => {
  
  beforeAll( () => {
    return User.destroy({
        truncate: true,
        cascade: true,
        restartIdentity: true
    }) 
  })

  newUser = newUser => {
    describe('POST /register', () => {
      test('Should succesfully create a new users', done => {
        request(app).post('/api/v1/register')
          .set('Content-Type','application/json')
          .send({
            name: newUser.name, 
            email: newUser.email,
            password: newUser.password
          })
          .then(res => {
            expect(res.statusCode).toEqual(201)
            expect(res.body.status).toEqual('success')
            expect(res.body.data).toHaveProperty('access_token')
            done()
          })
          .catch(error => console.log(error.message))
      })
    })
  }

  newUser(user1)
  newUser(user2)

  login = (userLogin) => {
    describe('POST /login', () => {
        test('Should succesfully login', done => {
          request(app).post('/api/v1/users/login')
          .set('Content-Type','application/json')
          .send({
              email: userLogin.email,
              password: userLogin.password
          })
          .then(res => {
              expect(res.statusCode).toEqual(200)
              expect(res.body.status).toEqual('success')
              expect(res.body.data).toHaveProperty('access_token')
              getToken = res.body.data.access_token
              done()
          })
          .catch(error => console.log(error.message))
        })
    })
  }

  login(user1)

  describe('PUT /users', () => {
    test('Should succesfully update data', done => {
      request(app).put('/api/v1/users')
      .set('Content-Type','application/json')
      .set('Authorization',getToken)
      .send({
          name: 'Muhammad Rasyid Hakim', 
          email: 'mrhakim@test.com',
      })
      .then(res => {
          expect(res.statusCode).toEqual(200)
          expect(res.body.status).toEqual('success')
          expect(res.body.data.data.name).toEqual('Muhammad Rasyid Hakim')
          expect(res.body.data.data.email).toEqual('mrhakim@test.com')
          done()
      })
      .catch(e => console.log(e.message))
    })
  })

  describe('GET /users', () => {
    test('Should succesfully show data from current token', done => {
      request(app).get('/api/v1/users')
      .set('Content-Type','application/json')
      .set('Authorization',getToken)
      .then(res => {
          console.log(res.body)
          expect(res.statusCode).toEqual(200)
          expect(res.body.status).toEqual('success')
          expect(res.body.data.name).toEqual('Muhammad Rasyid Hakim')
          done()
      })
      .catch(e => console.log(e.message))
    })
  })

  describe('GET /users/all', () => {
    test('Should succesfully show all user data', done => {
      request(app).get('/api/v1/users/all')
      .set('Content-Type','application/json')
      .then(res => {
          console.log(res.body)
          expect(res.statusCode).toEqual(200)
          expect((res.body.data).length).toEqual(2)
          done()
      })
      .catch(e => console.log(e.message))
    })
  })

  describe('DELETE /users', () => {
    test('Should succesfully delete data', done => {
      request(app).delete('/api/v1/users')
      .set('Content-Type','application/json')
      .set('Authorization',getToken)
      .then(res => {
          expect(res.statusCode).toEqual(200)
          expect(res.body.status).toEqual('success')
          expect(res.body).toHaveProperty('data')
          done()
      })
      .catch(e => console.log(e.message))
    })
  })

  describe('Token Invalid', () => {
    test('Should show error if token is not exist', done => {
      request(app).get('/api/v1/users')
      .set('Content-Type','application/json')
      .then(res => {
          expect(res.statusCode).toEqual(401)
          expect(res.body.status).toEqual('fail')
          expect(res.body).toHaveProperty('errors')
          done()
      })
      .catch(e => console.log(e.message))
    })
  })

  describe('Endpoint is not exist', () => {
    test('Should show error if endpoint is not exist', done => {
      request(app).get('/api/v1/users/12345')
      .set('Content-Type','application/json')
      .then(res => {
          expect(res.statusCode).toEqual(404)
          expect(res.body.status).toEqual('fail')
          expect(res.body).toHaveProperty('errors')
          done()
      })
      .catch(e => console.log(e.message))
    })
  })

})