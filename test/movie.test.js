const request = require('supertest');
const db = require('../models');
const app = require('../app');


describe('Movie API Collection', () => {
    beforeAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Movies", "Profiles", "Reviews" RESTART IDENTITY');
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Movies", "Profiles", "Reviews" RESTART IDENTITY');
    })

    describe('GET api/v1/movies/', () => {
        test('Status code 200 should successfully get all movies', (done) => {
            // .set('Authorization', access_token)
            request(app)
                .get('/api/v1/movies')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('data')
                    done()
                }).catch(console.log);
        });
    });

    describe('GET api/v1/movies/:movieId', () => {
        test('Status code 200 should successfully get one movies and average rating', (done) => {
            // .set('Authorization', access_token)
            request(app)
                .get('/api/v1/movies')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('data')
                    done()
                }).catch(console.log);
        });
    });
});