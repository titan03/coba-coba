const router = require('express').Router()
const Register = require('./Register')
const User = require('./User')
const Review = require('./Review');
const Movie = require('./Movie');
const Admin = require('./Admin/Admin');

const Documentation = require('./Documentation')

// ! Router for Admin
router.use('/', Admin)

router.use('/api/v1', Documentation)
router.use('/api/v1', Register)
router.use('/api/v1/users', User)

// ! Router for review
router.use('/api/v1/reviews', Review)

// ! Router for movie
router.use('/api/v1/movies', Movie)



module.exports = router