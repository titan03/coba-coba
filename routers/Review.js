const router = require('express').Router();
const Review = require('../controllers/Review');
const Auth = require('../middlewares/Auth');
const Owner = require('../middlewares/CheckOwner');
const MovieCheck = require('../middlewares/checkMovie');

router.post('/:id', Auth, MovieCheck('Movie'), Review.create) //! ragu ragu
router.get('/all', Auth, Review.getAll)
router.put('/:id', Auth, Owner('Review'), Review.update)
router.delete('/:id', Auth, Owner('Review'), Review.delete)
// router.get('/:movieId', Auth, Review.getDetailReview)

module.exports = router