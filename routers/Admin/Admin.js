const router = require('express').Router();
const Admin = require('../../controllers/Admin/loginAdmin');
const isLogin = require('../../middlewares/AuthAdmin');
const Movie = require('../../controllers/Admin/movieAdmin');


router.get('/admin', (req, res) => {
    res.render('home')
})

router.get('/login', (req, res) => {
    res.render('login', {
        err: req.query.err
    })
})

router.post('/login', Admin.loginAdmin)

router.get('/logout', (req, res) => {
    req.session.destroy(() => {
        console.log('islogout')
        res.redirect('/admin')
    })
})

// ! get form and create movie
router.get('/movies/add', isLogin, Movie.formCreate)
router.post('/movies/add', isLogin, Movie.create)

// ! get all movies
router.get('/movies', isLogin, Movie.getAll)

// ! edit movies by id
router.get('/movies/edit/:id', isLogin, Movie.editForm)
router.post('/movies/edit/:id', isLogin, Movie.editMovie)

//  ! delete movies by id
router.get('/movies/:id', Movie.delete)

module.exports = router