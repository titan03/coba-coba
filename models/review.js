'use strict';
module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.Sequelize.Model
  class Review extends Model {}

  Review.init({
    description: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: true
      }
    },
    rating: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: true
      }
    },
    title: DataTypes.STRING,
    UserId: DataTypes.INTEGER,
    MovieId: DataTypes.INTEGER
  }, {
    sequelize
  })
  // const Review = sequelize.define('Review', {

  // }, {});
  Review.associate = function (models) {
    // associations can be defined here
    Review.belongsTo(models.User, {
      foreignKey: 'UserId'
    })
    Review.belongsTo(models.Movie, {
      foreignKey: 'MovieId'
    })
  };
  return Review;
};