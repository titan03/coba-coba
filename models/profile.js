'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    image_url: {
      type: DataTypes.STRING,
      defaultValue: 'https://ik.imagekit.io/ckb21lc9cd/profile_image_SGfwkXuKe.png',
      validate: {
        notEmpty: {
          msg: `Input your image please!`
        }
      }
    },
    admin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      validate: {
        notEmpty: true
      }
    },
    id_user: DataTypes.INTEGER
  }, {});
  Profile.associate = function (models) {
    Profile.belongsTo(models.User, {
      foreignKey: `id`
    })
  };
  return Profile;
};