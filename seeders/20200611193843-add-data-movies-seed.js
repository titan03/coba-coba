'use strict';

const imdb = require('imdb-api')
const key = '829ccbd7'

const imdbID = ['tt2975590', 'tt0245429', 'tt6146586', 'tt6751668', 'tt0109830', 'tt2322441', 'tt1825683', 'tt0434409', 'tt4500922', 'tt1790809', 'tt0198781', 'tt0099685', 'tt1285016', 'tt2080374', 'tt0993846', 'tt1187043', 'tt0133093', 'tt7286456', 'tt0353969', 'tt0111161', 'tt0268978', 'tt4425200', 'tt0068646', 'tt1375666', 'tt4154796', 'tt3501632', 'tt0974959', 'tt0071562', 'tt3783958', 'tt6438096', 'tt4633694', 'tt2292955', 'tt1229238', 'tt0168122', 'tt4154756', 'tt2381249', 'tt5013056', 'tt0107290', 'tt5311514', 'tt0088247', 'tt0167260', 'tt8579674', 'tt0060196', 'tt4276820', 'tt8488126', 'tt0892769', 'tt9430698', 'tt1407050', 'tt0910970', 'tt1291584', 'tt3315342', 'tt0075148', 'tt0120737', 'tt0114709', 'tt0118799', 'tt0120689', 'tt3794354', 'tt0163651', 'tt1201607', 'tt1335975']



let promises = []
imdbID.forEach(element => {

  promises.push(imdb.get({
    id: element
  }, {
    apiKey: key
  }))
});

module.exports = {
  up: (queryInterface, Sequelize) => {

    return Promise.all(promises)

      .then(movies => {
        let arr = []
        movies.forEach(movie => {
          let objMovie = {
            title: movie.title,
            trailer: movie.trailer,
            poster: movie.poster,
            director: movie.director,
            released: movie.released,
            actor: movie.actors,
            languages: movie.languages,
            genre: movie.genres.split(',')[0],
            synopsis: movie.plot,
            createdAt: new Date(),
            updatedAt: new Date()
          }
          arr.push(objMovie)
        })
        return queryInterface.bulkInsert('Movies', arr)
      })
      .catch(err => {
        throw err
      })

    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Movies', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
      Example:
      
    */
  }
};