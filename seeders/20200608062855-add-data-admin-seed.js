'use strict';
const bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
        name: 'admin1',
        email: 'admin1@mail.com',
        encrypted_password: bcrypt.hashSync('admin', bcrypt.genSaltSync(10)),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'admin2',
        email: 'admin2@mail.com',
        encrypted_password: bcrypt.hashSync('admin', bcrypt.genSaltSync(10)),
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ], {});
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  }
};