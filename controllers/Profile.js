const {
  Profile
} = require('../models')

module.exports = {
  register: async (req, res, next) => {
    try {
      req.body.admin = false // This to ensure that admin always false in creation
      req.body.id_user = res.data.id // Get user id from previous res value
      const newProfile = await Profile.create(req.body)
      res.data.profile = newProfile
      res.status(201)
      next()
    } catch (error) {
      res.status(422)
      next(error)
    }
  }
}