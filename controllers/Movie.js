const {
    Movie,
    Review,
    User
} = require('../models');
const {
    Op
} = require('sequelize');
const Rating = require('../helper/CountRating');
class Movies {
    static async getAll(req, res, next) {
        let movie = req.query.movie
        if (movie) {
            const movies = await Movie.findAndCountAll({
                where: {
                    [Op.or]: {
                        title: {
                            [Op.like]: `%${movie}%`
                        },
                        genre: {
                            [Op.like]: `%${movie}%`
                        }
                    }
                },
                limit: req.query.limit || 10,
                offset: (req.query.page - 1) * req.query.limit || 0
            }).catch((err) => {
                res.status(422);
                next(err)
            })

            if (movies) {
                res.status(200).json({
                    status: 'Success',
                    data: [movies]
                })
            }
        } else {
            const movies = await Movie.findAndCountAll({
                limit: req.query.limit || 10,
                offset: (req.query.page - 1) * req.query.limit || 0
            }).catch((err) => {
                res.status(422);
                next(err)
            })

            if (movies) {
                res.status(200).json({
                    status: 'Success',
                    data: [movies]
                })
            }
        }
    }

    static async getRatingMovie(req, res, next) {
        try {
            const movie = await Movie.findByPk(req.params.movieId, {
                include: [{
                    model: Review
                }]
            })
            let averageRating = await Rating(req.params.movieId)
            const data = {
                movie,
                averageRating
            }
            res.status(200).json({
                status: 'Success',
                data: [data]
            })
        } catch (err) {
            res.status(422);
            next(err)
        }
        // let movie;
        // Movie.findByPk(req.params.id, {
        //     include: [{
        //         model: Review,
        //         include: [User]
        //     }]
        // }).then((foundMovie) => {
        //     movie = foundMovie
        //     return movie.getAverageRating()
        // }).then((averageRating) => {
        //     movie.setDataValue('averageRating', averageRating)
        //     return Review.findOne({
        //         where: {
        //             MovieId: movie.id,
        //             UserId: req.user.id
        //         }
        //     })
        // }).catch((err) => {
        //     res.status(422);
        //     next(err)
        // })
    }

    /*
      static async getByGenre(req, res, next) {
            try {
                const movies = await Movie.findAll({
                    where: {
                        genre: req.body.genre
                    }
                })
                if (movies) {
                    res.status(200).json({
                        status: 'Success',
                        data: [movies]
                    })
                }
            } catch (error) {
                res.status(422);
                next(error)
            }
        }
     */

}

module.exports = Movies